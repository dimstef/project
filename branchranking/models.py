from django.db import models
from branches.models import Branch
from branchposts.models import Post, React
from django.db.models import Window, F, Sum
from django.db.models.functions import DenseRank
from django.db.models.signals import post_save, post_init, pre_save, m2m_changed
from django.dispatch import receiver
import functools
import math

class BranchRanking(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['ranked_branch','community'],
                                    name='unique_ranked_branch_community'),
        ]

    @property
    def rank_position(self):
        return BranchRanking.objects.filter(community=self.community,rank__gt=self.rank).count() + 1

    @property
    def ownership(self):
        community_score = BranchRanking.objects.filter(community=self.community).aggregate(Sum('rank'))
        if community_score['rank__sum'] != 0:
            return (self.rank / community_score['rank__sum'])*100
        else:
            return 0

    def update_rank(self):
        posts_in_community = self.ranked_branch.posts.filter(posted_to__in=[self.community])
        post_score = 0.0
        for post in posts_in_community:
            diff = post.reacts.filter(type="star").count() - post.reacts.filter(type="dislikes").count()
            react_score = math.log(abs(diff) + 2,2) if diff > 0 else math.log(abs(diff) + 5,5)
            comment_score = math.sqrt(post.replies.count())
            post_score += (comment_score + 1) * react_score
        self.rank = post_score
        self.save()

    ranked_branch = models.ForeignKey(Branch,on_delete=models.CASCADE,related_name="rank")
    community = models.ForeignKey(Branch,on_delete=models.CASCADE,related_name="branch_rankings")
    rank = models.DecimalField(max_digits=14, decimal_places=5,default=0.0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


@receiver(post_save, sender=Post)
def create_ranking_after_post(sender, created, instance, **kwargs):
    if created:
        for community in instance.posted_to.filter(branch_type=Branch.COMMUNITY):
            if not instance.poster.rank.filter(community=community):
                instance.poster.rank.create(ranked_branch=instance.poster,community=community)

@receiver(m2m_changed,sender=Post.posted_to.through)
def posted_to_fallback(sender, instance, **kwargs):
    action = kwargs.pop('action', None)
    pk_set = kwargs.pop('pk_set', None)
    if action == "post_add":
        for community in instance.posted_to.filter(branch_type=Branch.COMMUNITY):
            if not instance.poster.rank.filter(community=community):
                instance.poster.rank.create(ranked_branch=instance.poster, community=community)

@receiver(post_save, sender=React, dispatch_uid="update_branch_rank_by_react")
def update_post_score(sender, instance, **kwargs):
     for community in instance.post.posted_to.all():
         rank = instance.post.poster.rank.filter(ranked_branch=instance.post.poster,community=community)
         if rank.first():
             rank.first().update_rank()