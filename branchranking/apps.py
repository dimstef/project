from django.apps import AppConfig


class BranchrankingConfig(AppConfig):
    name = 'branchranking'
