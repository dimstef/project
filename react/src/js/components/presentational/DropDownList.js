import React, {
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import {useTheme} from 'emotion-theming';
import {CSSTransition} from 'react-transition-group';
import {
  UserContext,
} from '../container/ContextContainer';
import {MobileModal} from './MobileModal';
import {Modal, ToggleContent} from './Temporary';
import {SmallBranch} from './Branch';
import {useMediaQuery} from 'react-responsive';

export function DropdownList({
  type = 'text',
  component = null,
  options,
  defaultOption,
  name,
  setParams,
  params,
  label,
  changeCurrentBranch,
  setBranch,
  preview = true,
  showOnTop,
  children,
}) {
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)',
  });
  const didMountRef = useRef(false);
  const [selected, setSelected] = useState(defaultOption);
  const [isOpen, setOpen] = useState(false);
  const ref = useRef(null);
  const Component = component;
  const userContext = useContext(UserContext);
  const theme = useTheme();

  function handleClick(e, show) {
    if (!isDesktopOrLaptop) {
      setOpen(true);
      show();
    } else {
      setOpen(!isOpen);
    }
  }

  function handleHide(hide) {
    setOpen(false);
  }

  function handleOutsideClick(e) {
    if (ref.current) {
      if (!ref.current.contains(e.target)) {
        setOpen(false);
      }
    }
  }

  useEffect(() => {
    if (didMountRef.current) {
      if (type == 'text') {
        setParams({...params, [label]: selected});
      } else {
        if (changeCurrentBranch) {
          userContext.changeCurrentBranch(selected);
        } else {
          setBranch(selected);
        }
      }
    } else {
      didMountRef.current = true;
    }
  }, [selected]);

  useEffect(() => {
    document.addEventListener('click', handleOutsideClick);

    return () => {
      document.removeEventListener('click', handleOutsideClick);
    };
  }, []);

  function handleSelect(option) {
    setSelected(option);
    setOpen(false);
  }

  return (
    <ToggleContent
      toggle={(show) => (
        <div
          style={{position: 'relative', display: 'table'}}
          ref={ref}
          onClick={(e) => handleClick(e, show)}>
          {preview ? (
            <div id={`${name}-filter`} className="flex-fill filter-selector">
              {type == 'text' ? (
                <span style={{color: theme.textLightColor}}>
                  {selected.label}
                </span>
              ) : (
                <SmallBranch
                  branch={selected}
                  isLink={false}
                  hoverable={false}
                />
              )}
              <DownArrowSvg />
            </div>
          ) : (
            <div>{children}</div>
          )}

          {isOpen && isDesktopOrLaptop ? (
            <div
              className="flex-fill filter-dropdown"
              style={{
                backgroundColor: theme.backgroundColor,
                top: showOnTop ? 0 : null,
                bottom: showOnTop ? null : 0,
              }}>
              {options.map((op) => {
                let props = {
                  handleSelect: handleSelect,
                  setSelected: setSelected,
                  selected: selected,
                  option: op,
                };
                return type == 'text' ? (
                  <DropdownItem {...props} />
                ) : (
                  <Component {...props} />
                );
              })}
            </div>
          ) : null}
        </div>
      )}
      content={(hide) => (
        <Modal onClick={handleHide} isOpen={isOpen}>
          <CSSTransition
            in={isOpen}
            timeout={200}
            classNames="side-drawer"
            onExited={() => hide()}
            appear>
            <MobileModal>
              {options.map((op) => {
                let props = {
                  handleSelect: handleSelect,
                  setSelected: setSelected,
                  selected: selected,
                  option: op,
                };
                return type == 'text' ? (
                  <DropdownItem {...props} />
                ) : (
                  <Component {...props} />
                );
              })}
            </MobileModal>
          </CSSTransition>
        </Modal>
      )}
    />
  );
}

function DropdownItem({setSelected, handleSelect, selected, option}) {
  const theme = useTheme();
  let style =
    option.value == selected.value
      ? {backgroundColor: theme.borderColor}
      : null;

  return (
    <span
      style={{...style}}
      onClick={() => handleSelect(option)}
      className="filter-dropdown-item">
      {option.label}
    </span>
  );
}

function DownArrowSvg() {
  const theme = useTheme();

  return (
    <svg
      xmlnsXlink="http://www.w3.org/1999/xlink"
      version="1.1"
      x="0px"
      y="0px"
      viewBox="0 0 41.999 41.999"
      style={{
        enableBackground: 'new 0 0 41.999 41.999',
        height: 7,
        width: 7,
        transform: 'rotate(90deg)',
        fill: theme.textColor,
        paddingLeft: 6,
      }}
      xmlSpace="preserve">
      <path d="M36.068 20.176l-29-20A1 1 0 1 0 5.5.999v40a1 1 0 0 0 1.568.823l29-20a.999.999 0 0 0 0-1.646z" />
    </svg>
  );
}