import React, {
  useLayoutEffect,
  useRef,
  useState,
  useEffect,
  useContext,
} from 'react';
import ReactDOM from 'react-dom';
import {Link, withRouter, useLocation} from 'react-router-dom';
import {useTheme} from 'emotion-theming';
import {css} from '@emotion/core';
import {NavLink} from 'react-router-dom';
import {
  useSpring,
  useSprings,
  useTransition,
  animated,
  interpolate,
} from 'react-spring/web.cjs';
import {useDrag} from 'react-use-gesture';
import Linkify from 'linkifyjs/react';
import {FollowButton} from './Card';
import {Profile} from './SettingsPage';
import {DesktopProfile} from './ProfileViewer';
import {
  UserContext,
  ParentBranchDrawerContext,
} from '../container/ContextContainer';
import {PopUp} from './PreviewPost';

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

const followContainerCss = (theme) =>
  css({
    display: 'inline-flex',
    margin: 14,
    borderRadius: 5,
    fontSize: '1.4em',
  });

const followCss = (theme) =>
  css({
    display: 'inline',
    marginRight: 10,
    borderRadius: 5,
    cursor: 'pointer',
  });

export const MobileBranchPageWrapper = React.memo(
  function MobileBranchPageWrapper({branch, children}) {
    const theme = useTheme();
    const ref = useRef(null);
    const [left, setLeft] = useState(0);
    const [imageHeight, setImageHeight] = useState(0);

    let defaultBannerUrl = '/images/group_images/banner/default';
    let r = new RegExp(defaultBannerUrl);
    let isDefault = r.test(branch.branch_banner);

    useLayoutEffect(() => {
      let offsetLeft = ref.current.offsetLeft;
      setLeft(offsetLeft);

      // same height as width
      let height = ref.current.clientWidth;
      setImageHeight(height);
    }, [ref]);

    return (
      <div className="mobile-branch-front-page">
        <div style={{width: '100%', position: 'relative'}}>
          <div
            style={{
              display: 'inline-block',
              width: '100%',
              height: 'auto',
              position: 'relative',
              overflow: 'hidden',
              padding: '34.37% 0 0 0',
              backgroundColor: getRandomColor(),
            }}>
            <img
              style={{
                maxWidth: '100%',
                width: '100%',
                position: 'absolute',
                bottom: 0,
                objectFit: 'cover',
                top: '50%',
                right: '50%',
                transform: 'translate(50%, -50%)',
              }}
              src={isDefault ? null : branch.branch_banner}></img>
          </div>
          <img
            ref={ref}
            style={{
              width: '27%',
              height: imageHeight,
              objectFit: 'cover',
              borderRadius: '50%',
              position: 'absolute',
              bottom: '-40%',
              left: '3%',
              border: `4px solid ${theme.backgroundColor}`,
            }}
            src={branch.branch_image}></img>
        </div>
        <ImageNeighbour el={ref} branch={branch} />
        <div style={{margin: left}}>
          <Description description={branch.description} />
        </div>
        <div css={followContainerCss} className="center-items">
          <FollowInfo
            branch={branch}
            followersCount={branch.followers_count}
            followingCount={branch.following_count}
          />
        </div>
        <NavigationTabs branch={branch} />
        {children}
      </div>
    );
  },
);

function Description({description}) {
  return (
    <Linkify>
      <p className="text-wrap" style={{fontSize: '1.7rem', marginTop: 8}}>
        {description}
      </p>
    </Linkify>
  );
}

function FollowInfo({branch, followersCount, followingCount}) {
  const theme = useTheme();

  return (
    <>
      <Link
        to={{pathname: `/${branch.uri}/followers`, state: 'followers'}}
        style={{textDecoration: 'none'}}>
        <div css={followCss}>
          <span style={{color: '#2196f3'}}>{followersCount} </span>
          <span
            style={{
              fontWeight: 500,
              fontSize: '0.9em',
              color: theme.textLightColor,
            }}>
            Followers
          </span>
        </div>
      </Link>
      <Link
        to={{pathname: `/${branch.uri}/following`, state: 'following'}}
        style={{textDecoration: 'none'}}>
        <div css={followCss}>
          <span style={{color: '#2196f3'}}>{followingCount} </span>
          <span
            style={{
              fontWeight: 500,
              fontSize: '0.9em',
              color: theme.textLightColor,
            }}>
            Following
          </span>
        </div>
      </Link>
    </>
  );
}

function ImageNeighbour({el, branch}) {
  const [left, setLeft] = useState(0);

  useLayoutEffect(() => {
    let offsetLeft = el.current.offsetLeft;
    let width = el.current.width;
    let extraOffset = 20;
    setLeft(offsetLeft + width + extraOffset);
  }, [el]);

  return (
    <div style={{width: '100%', height: 80, position: 'relative'}}>
      <div style={{position: 'absolute', left: left}}>
        <Identifiers branch={branch} />
      </div>
    </div>
  );
}

function Identifiers({branch}) {
  return (
    <div
      className="flex-fill"
      style={{
        alignItems: 'center',
        WebkitAlignItesm: 'center',
        flexFlow: 'row wrap',
        WebkitFlexFlow: 'row wrap',
      }}>
      <div>
        <Name name={branch.name} />
        <Uri uri={branch.uri} />
      </div>
      <FollowButton branch={branch} style={{width: 'auto'}} />
    </div>
  );
}

function Name({name}) {
  return (
    <p
      className="text-wrap"
      style={{margin: 0, fontSize: '2rem', fontWeight: 'bold'}}>
      {name}
    </p>
  );
}

function Uri({uri}) {
  const theme = useTheme();

  return (
    <span style={{fontSize: '2em', color: theme.textLightColor}}>@{uri}</span>
  );
}

function NavigationTabs({branch}) {
  const theme = useTheme();
  return (
    <div
      className="flex-fill"
      style={{
        justifyContent: 'center',
        WebkitJustifyContent: 'center',
        alignItems: 'center',
        WebkitAlignItems: 'center',
        padding: 10,
      }}>
      <NavLink
        exact
        to={{pathname: `/${branch.uri}`, state: 'branch'}}
        style={{
          textDecoration: 'none',
          color: theme.textHarshColor,
          textAlign: 'center',
          fontWeight: 'bold',
          fontSize: '2rem',
          width: '100%',
        }}
        activeStyle={{borderBottom: '2px solid #2196f3', color: '#2196f3'}}>
        Posts
      </NavLink>
      <NavLink
        to={{pathname: `/${branch.uri}/branches`, state: 'branch'}}
        style={{
          textDecoration: 'none',
          color: theme.textHarshColor,
          textAlign: 'center',
          fontWeight: 'bold',
          fontSize: '2rem',
          width: '100%',
        }}
        activeStyle={{borderBottom: '2px solid #2196f3', color: '#2196f3'}}>
        {branch.branch_count > 0 ? branch.branch_count : 0} Branches
      </NavLink>
    </div>
  );
}

export function MobileParentBranch2({branch, children}) {
  return branch ? (
    <div style={{height: '100%'}}>{children}</div>
  ) : (
    <div>{children}</div>
  );
}
export function ProfileBubble({branch}) {
  const isDown = useRef(false);
  const userContext = useContext(UserContext);
  const parentBranchDrawerContext = useContext(ParentBranchDrawerContext);
  const [show, setShow] = useState(false);

  parentBranchDrawerContext.setShow = setShow;

  return (
    <ProfileDrawerWithRouter branch={branch} shown={show} setShown={setShow} />
  );
}

const to = (x) => ({x: x});

const ProfileDrawer = React.memo(
  function ProfileDrawer({shown, setShown, branch, history}) {
    const ref = useRef(null);
    const shouldDrag = useRef(true);
    const shouldShow = useRef(false);
    const userContext = useContext(UserContext);

    const bind = useDrag(
      ({
        down,
        movement: [mx, my],
        cancel,
        velocity,
        direction: [xDir, yDir],
      }) => {
        if (!shown && !shouldDrag.current) {
          cancel();
          return;
        }
        const trigger = velocity > 0.2 && xDir < 0;
        const isGone = trigger && !down;
        const x = isGone ? -window.innerWidth : down ? mx : 0;
        if (isGone) {
          set(() => to(-window.innerWidth));
          shouldShow.current = false;
          //setShown(false);
        } else {
          set({x: x});
        }
      },
      {axis: 'x'},
    );

    useEffect(() => {
      const unlisten = history.listen(() => {
        //set(()=>to(-window.innerWidth))
        shouldShow.current = false;
        setShown(false);
      });

      return () => {
        unlisten();
      };
    });

    function handleClick(e) {
      setShown(false);
    }

    function outSideClick(e) {
      shouldDrag.current = false;
      if (ref.current) {
        if (!ref.current.contains(e.target)) {
          shouldShow.current = false;
          setShown(false);
          //set(()=>to(-window.innerWidth))
        }
      }
    }

    return ReactDOM.createPortal(
      <PopUp shown={shown} setShown={setShown} noHeader darkBackground>
        <div
          css={(theme) => ({
            height: '100%',
            width: '100%',
            position: 'relative',
          })}>
          <div
            css={{position: 'absolute', top: 20, left: 20}}
            onClick={handleClick}>
            <ArrowLeftSvg />
          </div>
          <DesktopProfile branch={userContext.currentBranch} innerRef={ref} />
        </div>
      </PopUp>,
      document.getElementById('hidden-elements'),
    );
  },
  (prevProps, nextProps) => {
    return prevProps.shown == nextProps.shown;
  },
);

export const ProfileDrawerWithRouter = withRouter(ProfileDrawer);

const ArrowLeftSvg = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      version="1.1"
      x="0px"
      y="0px"
      viewBox="0 0 31.494 31.494"
      style={{enableBackground: 'new 0 0 31.494 31.494'}}
      css={(theme) => ({fill: theme.textHarshColor, width: 20, height: 20})}
      xmlSpace="preserve">
      <path d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554  c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587  c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z" />
    </svg>
  );
};
