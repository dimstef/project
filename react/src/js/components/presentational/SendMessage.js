import React, {useContext, useState} from 'react';
import ReactDOM from 'react-dom';
import {
  ChatRoomsContext,
  UserContext,
  SingularPostContext,
} from '../container/ContextContainer';
import {PopUp} from './PreviewPost';
import MoonLoader from 'react-spinners/MoonLoader';
import {useRooms} from '../container/ChatRoomsContainer';
import history from '../../history';
import axios from 'axios';

export function SendMessage({sendMessageShown, setSendMessageShown, post}) {
  const roomsContext = useContext(ChatRoomsContext);

  return ReactDOM.createPortal(
    <PopUp
      header="Send to"
      shown={sendMessageShown}
      setShown={setSendMessageShown}>
      <div css={{display: 'flex', flexFlow: 'column', width: '100%'}}>
        {roomsContext.rooms.map((r, i) => {
          return (
            <React.Fragment key={i}>
              <SendToRoom post={post} room={r} />
            </React.Fragment>
          );
        })}
      </div>
    </PopUp>,
    document.getElementById('hidden-elements'),
  );
}

function SendToRoom({room, post}) {
  const postsContext = useContext(SingularPostContext);
  const userContext = useContext(UserContext);
  const [loading, setLoading] = useState(false);

  function handleSendMessage(roomId) {
    setLoading(true);

    let message = null;
    if (message == null) {
      message = '';
    }
    const formData = new FormData();
    formData.append('message', message);
    formData.append('message_html', message);
    formData.append('author', userContext.currentBranch.id);
    formData.append('embedded_post', post.id);

    let uri = `/api/branches/${userContext.currentBranch.uri}/chat_rooms/${roomId}/messages/new/`;

    axios
      .post(uri, formData, {
        withCredentials: true,
        headers: {
          'Content-Type': 'multipart/form-data',
          'X-CSRFToken': getCookie('csrftoken'),
        },
      })
      .then((r) => {
        history.push(`/messages/${roomId}`);
        setLoading(false);
      });
  }

  return (
    <div
      css={{
        display: 'flex',
        marginTop: 5,
        alignItems: 'center',
        maxWidth: 400,
        justifyContent: 'center',
        padding: 10,
        boxSizing: 'border-box',
      }}>
      <img
        src={room.preview_image}
        css={{borderRadius: '50%', height: 36, width: 36, objectFit: 'cover'}}
      />
      <span css={{marginLeft: 10, flex: 1, fontSize: '1.5rem', marginLeft: 5}}>
        {room.preview_name}
      </span>
      <span
        style={{cursor: 'pointer'}}
        className="accept-btn"
        onClick={() => handleSendMessage(room.id)}>
        {loading ? 'Sending..' : 'Send'}
      </span>
    </div>
  );
}
