import React, {
  useEffect,
  useLayoutEffect,
  useState,
  useRef,
  useContext,
} from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import {useMediaQuery} from 'react-responsive';
import MoonLoader from 'react-spinners/MoonLoader';
import {css} from '@emotion/core';
import {useTheme} from 'emotion-theming';
import {UserContext} from '../container/ContextContainer';
import {FadeImage} from './FadeImage';
import {CircularSkeletonList} from './SkeletonBranchList';
import {AddBranch, useBranchRequest} from './BranchesPage';
import RoutedHeadline from './RoutedHeadline';
import {PopUp} from './PreviewPost';
import {CircularBranch, SquareBranch} from './Branch';
import {PlusSvg, BranchesSvg, ConnectSvg} from './Svgs';
import history from '../../history';
import axios from 'axios';
import axiosRetry from 'axios-retry';

axiosRetry(axios, {
  retries: 15,
  retryDelay: axiosRetry.exponentialDelay,
});

const requestInfo = (theme) =>
  css({
    padding: 30,
    fontSize: '2rem',
    fontWeight: '500',
    backgroundColor: theme.backgroundLightColor,
    boxShadow: theme.postFloatingButtonShadow,
    borderRadius: 100,
    wordBreak: 'break-word',
    textAlign: 'center',
    maxWidth: '70%',
  });

const container = (theme) =>
  css({
    display: 'flex',
    flexFlow: 'column',
    border: `1px solid ${theme.borderColor}`,
    width: '100%',
    maxWidth: '70%',
    margin: '0 auto',
    '@media (max-device-width:767px)': {
      maxWidth: '100%',
    },
  });

const rowContainer = () =>
  css({
    display: 'flex',
    flexFlow: 'column',
    justifyContent: 'space-around',
    flex: 1,
    paddingBottom: 60,
    position: 'relative',
  });
const branchRow = (isMobile) =>
  css({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexFlow: 'row wrap',
    padding: '10px 0',
    a: {
      textDecoration: 'none',
    },
  });

const skeletonRow = (isMobile) =>
  css({
    display: 'inline-flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flex: '1 1 auto',
    width: '100%',
    margin: isMobile ? 0 : 20,
  });

const header = (theme, isMobile) =>
  css({
    color: theme.textHarshColor,
    margin: '5px 0',
    fontSize: isMobile ? '1.7rem' : '2rem',
    display: 'flex',
    justifyContent: isMobile ? 'center' : 'start',
    textAlign: 'center',
    marginLeft: 20,
    '@media (max-device-width:767px)': {
      marginLeft: 0,
    },
  });

const topRow = () =>
  css({
    width: '100%',
    display: 'flex',
    flexFlow: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    boxShadow:
      '0 1px 1px rgba(0,0,0,0.15), 0 2px 2px rgba(0,0,0,0.15), 0 4px 4px rgba(0,0,0,0.15), 0 5px 5px rgba(0,0,0,0.15)',
  });

const loadMore = (theme) =>
  css({
    width: '100%',
    boxSizing: 'border-box',
    padding: 15,
    border: 0,
    borderRadius: 50,
    backgroundColor: theme.backgroundDarkColor,
    color: theme.textColor,
    fontWeight: 'bold',
  });

const imageContainer = () =>
  css({
    display: 'flex',
    flexFlow: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  });

const name = (theme) =>
  css({
    color: theme.textLightColor,
    fontSize: '1.5rem',
    fontWeight: 500,
  });

export function DiscoverBranchesPage({
  match,
  branch,
  endpoint = 'search',
  showTop = true,
  withHeadline = false,
}) {
  return (
    <ResponsiveBranchRows
      uri={match.params ? match.params.uri : branch.uri}
      showTop={showTop}
      endpoint={endpoint}
      withHeadline={withHeadline}
    />
  );
}

function ResponsiveBranchRows({uri, showTop, endpoint, withHeadline}) {
  const userContext = useContext(UserContext);
  const [branch, setBranch] = useState(null);
  const [
    postRequest,
    requestStatus,
    submitted,
    isAlreadyConnected,
  ] = useBranchRequest(branch);
  const [right, setRight] = useState(0);
  const [siblings, setSiblings] = useState(null);
  const [connectShown, setConnectShown] = useState(false);
  const ref = useRef(null);
  const isMobile = useMediaQuery({
    query: '(max-device-width: 767px)',
  });

  async function getInitBranch() {
    let response = await axios.get(`/api/v1/branches/${uri}/`);
    setBranch(response.data);
    getSiblings(response.data);
  }

  async function getSiblings(branch) {
    let response = await axios.get(
      `/api/v1/branches/${branch.uri || branch}/siblings/`,
    );
    setSiblings(response.data.results);
  }

  useLayoutEffect(() => {
    if (ref.current) {
      setRight(ref.current.getBoundingClientRect().left + 20);
    }
  }, [ref]);

  useEffect(() => {
    getInitBranch();
  }, [uri]);

  return (
    <div
      css={(theme) => container(theme)}
      className="big-main-column"
      style={{padding: 0, height: '100%'}}>
      {showTop ? (
        <div css={topRow}>
          <FadeImage
            src={branch ? branch.branch_image : null}
            className="round-picture"
            style={{objectFit: 'cover', height: 80, width: 80}}
          />
        </div>
      ) : null}
      {withHeadline ? (
        <RoutedHeadline
          to={`/${branch ? branch.uri : null}`}
          headline={branch ? `${branch.name}'s branches` : null}
        />
      ) : null}

      <div css={rowContainer} ref={ref}>
        {branch ? (
          <>
            <BranchRow
              type="parents"
              branch={branch}
              key="parents"
              endpoint={endpoint}
            />
            <BranchRow
              type="children"
              branch={branch}
              key="children"
              endpoint={endpoint}
            />
            <BranchRow
              type="siblings"
              branch={branch}
              key="siblings"
              endpoint={endpoint}
            />
            {userContext.isAuth && !isAlreadyConnected ? (
              <div
                css={{
                  width: '100%',
                  display: 'flex',
                  flexFlow: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <div
                  onClick={() => setConnectShown(true)}
                  css={(theme) => ({
                    display: 'flex',
                    flexFlow: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 10,
                    borderRadius: 100,
                    backgroundColor: theme.backgroundLightColor,
                    boxShadow: theme.postFloatingButtonShadow,
                    width: '50%',
                    cursor: 'pointer',
                    background:
                      'linear-gradient(90deg,rgba(0, 188, 218, 0.98) 0%,rgb(130, 65, 230) 100%)',
                  })}>
                  <ConnectSvg
                    css={(theme) => ({height: 40, width: 40, fill: 'white'})}
                  />
                  <h1
                    css={{
                      wordBreak: 'break-word',
                      textAlign: 'center',
                      color: 'white',
                      fontSize: '1.5rem',
                    }}>
                    Connect here
                  </h1>
                </div>
              </div>
            ) : null}
          </>
        ) : null}
      </div>
      {ReactDOM.createPortal(
        branch ? (
          <PopUp
            shown={connectShown}
            setShown={setConnectShown}
            header={`Connect to ${branch.name}`}>
            <Connect branch={branch} />
          </PopUp>
        ) : null,
        document.getElementById('hidden-elements'),
      )}
    </div>
  );
}

function Connect({branch}) {
  const userContext = useContext(UserContext);
  const [type, setType] = useState('child');
  const [
    postRequest,
    requestStatus,
    submitted,
    isAlreadyConnected,
  ] = useBranchRequest(branch);

  return (
    <div
      css={(theme) => ({
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexFlow: 'column',
        flexGrow: 1,
        height: '100%',
        justifyContent: 'space-evenly',
      })}>
      {requestStatus == 'on hold' ? (
        <div css={requestInfo}>Request pending, waiting for approval</div>
      ) : requestStatus == 'declined' ? (
        <div css={requestInfo}>
          Your request was declined by the branch owner
        </div>
      ) : requestStatus == 'accepted' ? (
        <div css={requestInfo}>
          Request was accepted! Results may be available upon refresh
        </div>
      ) : (
        <>
          <ConnectAsMoreSpecific
            branch={branch}
            onClick={() => postRequest('child')}
          />
          <ConnectAsMoreGeneric
            branch={branch}
            onClick={() => postRequest('parent')}
          />
        </>
      )}
    </div>
  );
}

function ConnectAsMoreSpecific({branch, onClick}) {
  const userContext = useContext(UserContext);
  const isMobile = useMediaQuery({
    query: '(max-device-width: 767px)',
  });

  return (
    <div
      onClick={onClick}
      css={(theme) => ({
        display: 'flex',
        flexFlow: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1,
        width: '80%',
        borderRadius: 25,
        margin: 20,
        cursor: 'pointer',
        backgroundColor: theme.backgroundLightColor,
        boxShadow: theme.postFloatingButtonShadow,
      })}>
      <span css={{fontSize: '2rem', marginBottom: 10}}>
        Connect as more <b>specific</b>
      </span>
      <div css={{position: 'relative', width: 'max-content'}}>
        <img
          src={branch.branch_image}
          css={{
            height: 50,
            width: 50,
            objectFit: 'cover',
            borderRadius: '50%',
            margin: '5px 0',
          }}
        />
        <div css={{display: 'flex', justifyContent: 'space-between'}}>
          <div
            css={{
              display: 'flex',
              flexFlow: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              transform: 'translateX(-10px)',
            }}>
            <div
              css={(theme) => ({
                height: isMobile ? 50 : 80,
                width: 5,
                backgroundColor: theme.primaryColor,
                position: 'relative',
                display: 'flex',
                justifyContent: 'center',
                borderRadius: 50,
              })}></div>
            <img
              src={userContext.currentBranch.branch_image}
              css={{
                height: 50,
                width: 50,
                objectFit: 'cover',
                borderRadius: '50%',
                margin: '5px 0',
              }}
            />
          </div>
          <div
            css={(theme) => ({
              height: isMobile ? 25 : 40,
              width: 5,
              backgroundColor: theme.primaryColor,
              transform: 'rotate(-25deg)',
              borderRadius: 50,
            })}></div>
        </div>
      </div>
    </div>
  );
}

function ConnectAsMoreGeneric({branch, onClick}) {
  const userContext = useContext(UserContext);
  const isMobile = useMediaQuery({
    query: '(max-device-width: 767px)',
  });

  return (
    <div
      onClick={onClick}
      css={(theme) => ({
        display: 'flex',
        flexFlow: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1,
        width: '80%',
        borderRadius: 25,
        margin: 20,
        cursor: 'pointer',
        backgroundColor: theme.backgroundLightColor,
        boxShadow: theme.postFloatingButtonShadow,
      })}>
      <span css={{fontSize: '2rem', marginBottom: 10}}>
        Connect as more <b>generic</b>
      </span>
      <div
        css={{
          position: 'relative',
          width: 'max-content',
          display: 'flex',
          flexFlow: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <img
          src={userContext.currentBranch.branch_image}
          css={{
            height: 50,
            width: 50,
            objectFit: 'cover',
            borderRadius: '50%',
            margin: '5px 0',
          }}
        />
        <Line width={5} height={isMobile ? 50 : 80} />
        <div css={{display: 'flex', flexFlow: 'column'}}>
          <img
            src={branch.branch_image}
            css={{
              height: 50,
              width: 50,
              objectFit: 'cover',
              borderRadius: '50%',
              margin: '5px 0',
            }}
          />
          <div css={{display: 'flex', justifyContent: 'space-between'}}>
            <Line width={5} height={isMobile ? 25 : 40} rotation={25} />
            <Line width={5} height={isMobile ? 25 : 40} rotation={-25} />
          </div>
        </div>
      </div>
    </div>
  );
}

function Line({width, height, rotation = 0, left = 0, right = 0}) {
  return (
    <div
      css={(theme) => ({
        height: height,
        width: width,
        backgroundColor: theme.primaryColor,
        left: left,
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        borderRadius: 50,
        transform: `rotate(${rotation}deg)`,
        right: right,
      })}></div>
  );
}

function useBranches(type = 'children', branch) {
  const [prevBranch, setPrevBranch] = useState(branch);
  const [branches, setBranches] = useState(null);
  const [next, setNext] = useState(null);
  const [hasMore, setHasMore] = useState(true);
  const [loading, setLoading] = useState(false);

  async function getBranches() {
    setLoading(true);
    let response = await axios.get(
      next ? next : `/api/branches/${branch.uri || branch}/${type}/`,
    );
    setLoading(false);

    if (!response.data.next) {
      setHasMore(false);
    }
    setNext(response.data.next);

    // This is needed to prevent stale state
    if (branch != prevBranch) {
      setBranches(response.data.results);
    } else {
      setBranches(
        branches
          ? [...branches, ...response.data.results]
          : response.data.results,
      );
    }
    setPrevBranch(branch);
  }

  useEffect(() => {
    setBranches(null);
    setNext(null);
    setHasMore(true);
    getBranches();
  }, [branch]);

  return [branches, next, hasMore, getBranches, loading];
}

function BranchRow({type, branch, endpoint}) {
  const ref = useRef(null);
  const userContext = useContext(UserContext);
  const [branches, next, hasMore, getBranches, loading] = useBranches(
    type,
    branch,
  );
  const theme = useTheme();

  const isMobile = useMediaQuery({
    query: '(max-device-width: 767px)',
  });

  let infoText;
  if (type == 'children') {
    infoText = `More specific communities than ${branch.name}`;
  } else if (type == 'parents') {
    infoText = `More generic communities than ${branch.name}`;
  } else {
    infoText = `Communities probably related to ${branch.name}`;
  }

  function handleClick(branch) {
    history.push(`/search/${branch.uri}`);
  }

  return branches ? (
    <div
      css={(theme) => ({
        margin: '15px 20px',
        boxSizing: 'border-box',
        borderRadius: 20,
        padding: '5px 0',
        display: 'flex',
        alignItems: 'flex-start',
        flexFlow: 'column',
      })}>
      <div css={{width: '100%', padding: 5, boxSizing: 'border-box'}}>
        <h1 css={(theme) => header(theme, isMobile)}>{infoText}</h1>
        {branches.length > 0 ? (
          <div css={() => branchRow(isMobile)}>
            {branches
              ? branches.map((b, i) => {
                  return (
                    <React.Fragment key={i}>
                      <CircularBranch branch={b} endpoint={endpoint} />
                    </React.Fragment>
                  );
                })
              : null}
          </div>
        ) : (
          <div css={{display: 'flex', flexFlow: 'column'}}>
            <p
              css={(theme) => ({
                color: theme.textColor,
                fontSize: '1.4rem',
                textAlign: 'center',
              })}>
              {type == 'siblings'
                ? 'No related communites were found :('
                : 'No one is here yet. Expand your community by connecting with other communities'}
            </p>
          </div>
        )}
      </div>
      {hasMore ? (
        <div
          css={{
            display: 'flex',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {loading ? (
            <div
              className="flex-fill load-spinner-wrapper"
              css={{justifyContent: 'center', margin: '0 30px'}}>
              <MoonLoader
                sizeUnit={'px'}
                size={20}
                color={theme.textLightColor}
                loading={true}
              />
            </div>
          ) : (
            <button css={loadMore} onClick={getBranches}>
              Load more
            </button>
          )}
        </div>
      ) : null}
    </div>
  ) : (
    <div css={() => skeletonRow(isMobile)}>
      <CircularSkeletonList
        count={isMobile ? 3 : 5}
        dimensions={isMobile ? 70 : 100}
      />
    </div>
  );
}

function BranchRow2({type, branch, endpoint, vertical}) {
  const ref = useRef(null);
  const userContext = useContext(UserContext);
  const [branches, next, hasMore, getBranches, loading] = useBranches(
    type,
    branch,
  );
  const theme = useTheme();

  const isMobile = useMediaQuery({
    query: '(max-device-width: 767px)',
  });

  let infoText;
  if (type == 'children') {
    infoText = `More specific communities than ${branch.name}`;
  } else if (type == 'parents') {
    infoText = `More generic communities than ${branch.name}`;
  } else {
    infoText = `Communities probably related to ${branch.name}`;
  }

  return branches && branches.length > 0 ? (
    <InfiniteScroll
      hasMore={hasMore}
      loadMore={getBranches}
      dataLength={branches.length}
      vertical={vertical}>
      <div>
        <h1 css={(theme) => header(theme, isMobile)}>{infoText}</h1>
        <div css={vertical ? branchColumn : branchRow}>
          {branches.map((b) => {
            return (
              <React.Fragment key={b.uri}>
                <CircularBranch branch={b} endpoint={endpoint} />
              </React.Fragment>
            );
          })}
        </div>
      </div>
    </InfiniteScroll>
  ) : null;
}

function InfiniteScroll({
  hasMore,
  loadMore,
  dataLength,
  vertical = true,
  bss = null,
  children,
}) {
  const ref = useRef(null);
  const [scroll, setScroll] = useState(0);
  const [itemCount, setItemCount] = useState(0);

  function listenHorizontal(e) {
    if (vertical) {
      setScroll(e.currentTarget.scrollTop);
    } else {
      setScroll(e.currentTarget.scrollLeft);
    }
  }

  useEffect(() => {
    let trigger = vertical
      ? ref.current.scrollTop - ref.current.clientHeight - 250
      : ref.current.scrollWidth - ref.current.clientWidth - 250;

    if (scroll > trigger) {
      if (hasMore && (dataLength != itemCount || dataLength == 0)) {
        loadMore();
        setItemCount(dataLength);
      }
    }
  }, [scroll]);

  useEffect(() => {
    if (ref.current) {
      ref.current.addEventListener('scroll', listenHorizontal);

      return () => {
        ref.current.removeEventListener('scroll', listenHorizontal);
      };
    }
  }, [ref]);

  return (
    <div style={{overflow: 'auto'}} css={bss} ref={ref}>
      {children}
    </div>
  );
}
