import React, {useContext, useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {
  ChatRoomsContext,
  UserContext,
  SingularPostContext,
} from '../container/ContextContainer';
import {PopUp} from './PreviewPost';
import MoonLoader from 'react-spinners/MoonLoader';
import {SearchSvg} from './Svgs';
import history from '../../history';
import axios from 'axios';

export function Actions({actionsShown, setActionsShown, post}) {
  const [actions, setActions] = useState([]);
  const userContext = useContext(UserContext);

  async function getPostRemoveAction() {
    try {
      let uri = userContext.isAuth
        ? `/api/v1/post/${post.id}/actions/?branch=${userContext.currentBranch.uri}`
        : `/api/v1/post/${post.id}/actions/`;
      let response = await axios.get(uri);
      setActions(response.data);
    } catch (e) {}
  }

  useEffect(() => {
    if (actionsShown) {
      getPostRemoveAction();
    }
  }, [actionsShown]);

  return ReactDOM.createPortal(
    <PopUp header="Actions" shown={actionsShown} setShown={setActionsShown}>
      <div css={{display: 'flex', flexFlow: 'column', width: '100%'}}>
        {actions.length == 0 ? (
          <h1 css={{padding: 10}}>This post has no available actions</h1>
        ) : null}
        {actions.map((a, i) => {
          return (
            <React.Fragment key={i}>
              <Action action={a} />
            </React.Fragment>
          );
        })}
      </div>
    </PopUp>,
    document.getElementById('hidden-elements'),
  );
}

function Action({action}) {
  const userContext = useContext(UserContext);
  const [loading, setLoading] = useState(false);
  const [agree_percentage, setAgreePercentage] = useState(
    action.agree_percentage,
  );
  const [vote, setVote] = useState(action.vote);

  async function handleVote(type = 'yes') {
    if (!userContext.isAuth) {
      history.push('/login');
    }
    setLoading(true);

    let agreeToRemove = null;
    let disagreeToRemove = null;

    if (type == 'yes') {
      setVote('agreed');
      agreeToRemove = [userContext.currentBranch.id];
      //agreeToRemove.push(userContext.currentBranch.id)
    } else {
      setVote('disagreed');
      disagreeToRemove = [userContext.currentBranch.id];
      //disagreeToRemove.push(userContext.currentBranch.id)
    }

    const formData = new FormData();
    if (agreeToRemove) {
      formData.append('agreed', agreeToRemove);
    } else {
      formData.append('disagreed', disagreeToRemove);
    }

    let uri = `/api/v1/actions/remove_post/${action.surrogate}/`;

    axios
      .patch(uri, formData, {
        withCredentials: true,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then((r) => {
        setAgreePercentage(r.data.agree_percentage);
        setLoading(false);
      });
  }

  return (
    <div
      css={{display: 'flex', padding: 10, marginTop: 5, alignItems: 'center'}}>
      {/*<SearchSvg css={theme=>({height:50,width:50,fill:theme.textColor})}/>*/}

      <div
        css={{
          display: 'flex',
          flexFlow: 'column',
          marginLeft: 10,
          width: '100%',
          justifyContent: 'center',
        }}>
        <div>
          <h2
            css={{
              display: 'flex',
              alignItems: 'center',
              margin: '5px 0',
              textAlign: 'center',
            }}>
            <span css={{fontWeight: 500}}>
              Should this post be removed from
            </span>
            <span css={{marginLeft: 5, fontWeight: 'bold'}}>
              {action.from_community.name}?
            </span>
          </h2>
        </div>
        <h2
          css={{
            display: 'flex',
            alignItems: 'center',
            margin: '5px 0',
            textAlign: 'center',
          }}>
          Pass threshold: 50%
        </h2>
        <div
          css={(theme) => ({
            position: 'relative',
            width: '100%',
            height: 25,
            borderRadius: 100,
            overflow: 'hidden',
            border: `1px solid ${theme.borderColor}`,
          })}>
          <div
            css={(theme) => ({
              position: 'absolute',
              left: 0,
              backgroundColor: theme.primaryColor,
              width: `${agree_percentage}%`,
              height: '100%',
              transition: 'width 0.5s',
            })}></div>
          <span
            css={{
              position: 'absolute',
              margin: '0 auto',
              fontWeight: 'bold',
              width: '100%',
              height: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {Number(agree_percentage.toFixed(1))}% said yes
          </span>
        </div>
        {vote ? (
          <div
            css={(theme) => ({
              width: 'max-content',
              display: 'flex',
              justifyContent: 'center',
              padding: '5px 15px',
              alignSelf: 'center',
              alignItems: 'center',
              backgroundColor: vote == 'agreed' ? '#4CAF50' : '#ff9800',
              marginTop: 20,
              borderRadius: 100,
            })}>
            <span
              css={{color: 'white', fontWeight: 'bold', fontSize: '1.4rem'}}>
              You have already {vote}
            </span>
          </div>
        ) : null}

        <div
          css={{
            display: 'flex',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            margin: '20px 0',
          }}>
          <button
            className="accept-btn"
            css={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onClick={() => handleVote('yes')}>
            Yes
          </button>
          <div
            css={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              flexFlow: 'column',
            }}>
            <span
              css={{
                fontWeight: 'bold',
                fontSize: '2rem',
                '@media (max-width:767px)': {fontSize: '1.4rem'},
              }}>
              {Number(action.vote_power.toFixed(1))}% vote power
            </span>
            <span css={{fontSize: '1.2rem', marginTop: 3, padding: '0 20px'}}>
              Become an active member of <b>{action.from_community.name}</b> to
              increase your vote power
            </span>
          </div>
          <button
            className="accept-btn"
            css={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onClick={() => handleVote('no')}>
            No
          </button>
        </div>
      </div>
    </div>
  );
}
