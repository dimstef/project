import React, {
  useContext,
  useEffect,
  useState,
  useCallback,
  useRef,
} from 'react';
import ReactDOM from 'react-dom';
import escapeHtml from 'escape-html';
import {Editable, withReact, useSlate, Slate} from 'slate-react';
import {Editor, Transforms, Text, createEditor} from 'slate';
import {withHistory} from 'slate-history';
import {Button, Icon, Toolbar} from './RichEditorComponents';
import {PopUp} from './PreviewPost';
import MoonLoader from 'react-spinners/MoonLoader';
import history from '../../history';
import axios from 'axios';

function isMobile() {
  if (/Mobi|Android/i.test(navigator.userAgent)) {
    return true;
  }
  return false;
}

export default function RichEditor({
  editorShown,
  setEditorShown,
  value,
  setValue,
  editor,
  post,
}) {
  const _isMobile = isMobile();

  return ReactDOM.createPortal(
    <PopUp
      header="Create your post"
      shown={editorShown}
      setShown={setEditorShown}
      zIndex={1005}>
      <div
        css={{
          display: 'flex',
          flexFlow: 'column',
          width: '100%',
          fontSize: '1.6rem',
          padding: 10,
          boxSizing: 'border-box',
          blockquote: {
            backgroundColor: '#e8e8e812',
            padding: 5,
            borderRadius: 3,
          },
        }}>
        {_isMobile ? (
          <SimpleEditor value={value} setValue={setValue} />
        ) : (
          <RichTextExample value={value} setValue={setValue} editor={editor} />
        )}
        <button
          className="accept-btn"
          onClick={() => setEditorShown(false)}
          css={{width: 'max-content', margin: 0, marginTop: 10}}>
          Save draft and close fancy editor
        </button>
      </div>
    </PopUp>,
    document.getElementById('hidden-elements'),
  );
}

function SimpleEditor({value, setValue}) {
  const ref = useRef(null);

  function handleChange(e) {
    setValue(e.target.innerText);
  }

  useEffect(() => {
    if (ref.current) {
      ref.current.focus();
      ref.current.textContent = value;
    }
  }, [ref]);

  return (
    <div
      contentEditable
      onInput={handleChange}
      ref={ref}
      css={{
        padding: '20px 0 30px 0',
        whiteSpace: 'pre-wrap',
        wordWrap: 'break-word',
      }}
      value={value}
      data-placeholder={value ? null : 'Type your thoughts...'}></div>
  );
}

const HOTKEYS = {
  'mod+b': 'bold',
  'mod+i': 'italic',
  'mod+u': 'underline',
  'mod+`': 'code',
};

const LIST_TYPES = ['numbered-list', 'bulleted-list'];

const serialize = (node) => {
  if (Text.isText(node)) {
    let markWrapper = escapeHtml(node.text);
    if (node.bold) {
      markWrapper = `<strong>${markWrapper}<strong/>`;
    }
    if (node.code) {
      markWrapper = `<code>${markWrapper}<code/>`;
    }
    if (node.underline) {
      markWrapper = `<u>${markWrapper}<u/>`;
    }
    if (node.italic) {
      markWrapper = `<em>${markWrapper}<em/>`;
    }
    return markWrapper;
  }

  const children = node.children.map((n) => serialize(n)).join('');

  switch (node.type) {
    case 'block-quote':
      return `<blockquote><p>${children}</p></blockquote>`;
    case 'paragraph':
      return `<p>${children}</p>`;
    case 'link':
      return `<a href="${escapeHtml(node.url)}">${children}</a>`;
    case 'numbered-list':
      return `<ol>${children}</ol>`;
    case 'list-item':
      return `<li>${children}</li>`;
    default:
      return children;
  }
};

const RichTextExample = ({value, setValue, editor}) => {
  const renderElement = useCallback((props) => <Element {...props} />, []);
  const renderLeaf = useCallback((props) => <Leaf {...props} />, []);

  function handleChange(value) {
    setValue(value);
  }

  return (
    <Slate
      editor={editor}
      value={value}
      onChange={(value) => handleChange(value)}>
      <Toolbar>
        <MarkButton format="bold" icon="format_bold" />
        <MarkButton format="italic" icon="format_italic" />
        {/*<MarkButton format="underline" icon="format_underlined" />*/}
        <MarkButton format="code" icon="code" />
        <BlockButton format="heading-one" icon="looks_one" />
        <BlockButton format="heading-two" icon="looks_two" />
        <BlockButton format="block-quote" icon="format_quote" />
        <BlockButton format="numbered-list" icon="format_list_numbered" />
        <BlockButton format="bulleted-list" icon="format_list_bulleted" />
      </Toolbar>
      <Editable
        renderElement={renderElement}
        renderLeaf={renderLeaf}
        placeholder="Type your thoughts…"
        spellCheck
        autoFocus
      />
    </Slate>
  );
};

const toggleBlock = (editor, format) => {
  const isActive = isBlockActive(editor, format);
  const isList = LIST_TYPES.includes(format);

  Transforms.unwrapNodes(editor, {
    match: (n) => LIST_TYPES.includes(n.type),
    split: true,
  });

  Transforms.setNodes(editor, {
    type: isActive ? 'paragraph' : isList ? 'list-item' : format,
  });

  if (!isActive && isList) {
    const block = {type: format, children: []};
    Transforms.wrapNodes(editor, block);
  }
};

const toggleMark = (editor, format) => {
  const isActive = isMarkActive(editor, format);

  if (isActive) {
    Editor.removeMark(editor, format);
  } else {
    Editor.addMark(editor, format, true);
  }
};

const isBlockActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: (n) => n.type === format,
  });

  return !!match;
};

const isMarkActive = (editor, format) => {
  const marks = Editor.marks(editor);
  return marks ? marks[format] === true : false;
};

const Element = ({attributes, children, element}) => {
  switch (element.type) {
    case 'block-quote':
      return <blockquote {...attributes}>{children}</blockquote>;
    case 'bulleted-list':
      return <ul {...attributes}>{children}</ul>;
    case 'heading-one':
      return <h1 {...attributes}>{children}</h1>;
    case 'heading-two':
      return <h2 {...attributes}>{children}</h2>;
    case 'list-item':
      return <li {...attributes}>{children}</li>;
    case 'numbered-list':
      return <ol {...attributes}>{children}</ol>;
    default:
      return <p {...attributes}>{children}</p>;
  }
};

const Leaf = ({attributes, children, leaf}) => {
  if (leaf.bold) {
    children = <strong>{children}</strong>;
  }

  if (leaf.code) {
    children = <code>{children}</code>;
  }

  if (leaf.italic) {
    children = <em>{children}</em>;
  }

  if (leaf.underline) {
    children = <u>{children}</u>;
  }

  return <span {...attributes}>{children}</span>;
};

const BlockButton = ({format, icon}) => {
  const editor = useSlate();
  return (
    <Button
      active={isBlockActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleBlock(editor, format);
      }}>
      <Icon>{icon}</Icon>
    </Button>
  );
};

const MarkButton = ({format, icon}) => {
  const editor = useSlate();
  return (
    <Button
      active={isMarkActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleMark(editor, format);
      }}>
      <Icon>{icon}</Icon>
    </Button>
  );
};

const initialValue = [{type: 'paragraph', children: [{text: ''}]}];

function SendToRoom({room, post}) {
  const postsContext = useContext(SingularPostContext);
  const userContext = useContext(UserContext);
  const [loading, setLoading] = useState(false);
}
