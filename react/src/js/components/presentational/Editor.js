import React, {useEffect, useState} from 'react';
import {Suspense, lazy} from 'react';
import {Node} from 'slate';
const RichEditor = React.lazy(() => import('./RichEditor'));

const serialize = (nodes) => {
  return nodes.map((n) => Node.string(n)).join(' \n');
};

function isMobile() {
  if (/Mobi|Android/i.test(navigator.userAgent)) {
    return true;
  }
  return false;
}

export function CustomEditor({
  onInput,
  onKeyDown,
  placeholder,
  className,
  style,
  editorRef = null,
  onBlur,
  files,
  setFiles = () => {},
  autoFocus = false,
  value,
  setValue,
  editor,
}) {
  const _isMobile = isMobile();
  const [editorShown, setEditorShown] = useState(false);
  const [editorPlaceHolder, setPlaceHolder] = useState(placeholder);

  function handleInput(e) {
    if (e.target.innerText) {
      e.target.dataset.divPlaceholderContent = 'true';
    } else {
      delete e.target.dataset.divPlaceholderContent;
    }
    onInput(e);
  }

  function onPaste(event) {
    event.preventDefault();
    var items = (event || event.originalEvent).clipboardData.items;
    var text = (event || event.originalEvent).clipboardData.getData(
      'text/plain',
    );
    document.execCommand('insertHTML', false, text);

    for (var index in items) {
      var item = items[index];
      if (item.kind === 'file') {
        var blob = item.getAsFile();
        setFiles([...files, blob]);
      }
    }
  }

  function onDrop() {
    //
  }

  function onFocus() {
    setEditorShown(true);
  }

  /*useEffect(()=>{
        if(editorRef.current && autoFocus){
            editorRef.current.focus();
        }
    },[editorRef])*/

  useEffect(() => {
    let draft = _isMobile ? value : serialize(value).replace(/\s+/g, ' ');
    let wordCount = draft.split(' ').length;
    if (editorRef.current) {
      if (wordCount > 1 || draft.length > 0) {
        editorRef.current.textContent = `You have typed ${wordCount} words! Click here to view.`;
        setPlaceHolder(null);
      } else {
        editorRef.current.textContent = null;
        setPlaceHolder(placeholder);
      }
    }
  }, [value]);

  return (
    <>
      <div
        onPaste={onPaste}
        onDrop={onDrop}
        contentEditable
        className={className}
        ref={editorRef}
        onInput={handleInput}
        onKeyDown={onKeyDown}
        onBlur={onBlur}
        onFocus={onFocus}
        data-placeholder={editorPlaceHolder}
        style={style}
      />
      <Suspense fallback={null}>
        <RichEditor
          editorShown={editorShown}
          setEditorShown={setEditorShown}
          value={value}
          setValue={setValue}
          editor={editor}
        />
      </Suspense>
    </>
  );
}

export function MessengerEditor({
  onInput,
  onKeyDown,
  placeholder,
  className,
  style,
  editorRef = null,
  onBlur,
  files,
  setFiles = () => {},
  onFocus,
  autoFocus = false,
}) {
  function handleInput(e) {
    if (e.target.innerText) {
      e.target.dataset.divPlaceholderContent = 'true';
    } else {
      delete e.target.dataset.divPlaceholderContent;
    }
    onInput(e);
  }

  function onPaste(event) {
    event.preventDefault();
    var items = (event || event.originalEvent).clipboardData.items;
    var text = (event || event.originalEvent).clipboardData.getData(
      'text/plain',
    );
    document.execCommand('insertHTML', false, text);

    for (var index in items) {
      var item = items[index];
      if (item.kind === 'file') {
        var blob = item.getAsFile();
        setFiles([...files, blob]);
      }
    }
  }

  function onDrop() {
    //
  }

  useEffect(() => {
    if (editorRef.current && autoFocus) {
      editorRef.current.focus();
    }
  }, [editorRef]);

  return (
    <div
      onPaste={onPaste}
      onDrop={onDrop}
      contentEditable
      className={className}
      ref={editorRef}
      onInput={handleInput}
      onKeyDown={onKeyDown}
      onBlur={onBlur}
      onFocus={onFocus}
      data-placeholder={placeholder}
      style={style}
    />
  );
}
