import React, {Component, useContext, useState} from 'react';
import {Link, Redirect} from 'react-router-dom';
import {Helmet} from 'react-helmet';
import {Field, Form} from 'react-final-form';
import {css} from '@emotion/core';
import {useMediaQuery} from 'react-responsive';
import {
  AuthenticationInput,
  AuthenicationSave,
  AuthenticationWrapper,
} from './Forms';
import MoonLoader from 'react-spinners/MoonLoader';
import {UserContext} from '../container/ContextContainer';
import axios from 'axios';

export default function Register2() {
  let initialValues = {};

  const [loading, setLoading] = useState(false);

  const userContext = useContext(UserContext);
  const isMobile = useMediaQuery({
    query: '(max-device-width: 767px)',
  });

  async function onSubmit(values) {
    let errors = {};
    setLoading(true);

    try {
      let uri = '/rest-auth/registration/';
      let config = {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRFToken': getCookie('csrftoken'),
        },
      };
      let response = await axios.post(uri, values, config);
      localStorage.setItem('token', response.data.token);
      localStorage.setItem('justRegistered', 'true');
      setLoading(false);
    } catch (e) {
      setLoading(false);
      return e.response.data;
    }

    return errors;
  }

  return (
    <>
      <Helmet>
        <title>Register - Westeria</title>
        <meta name="description" content="Register to Westeria" />
        <link rel="canonical" href="https://westeria.app/register" />
      </Helmet>
      <AuthenticationWrapper
        header="Connect with us it's free!"
        logoSize={isMobile ? 40 : 100}>
        <div css={{padding: 10}}>
          <Form
            onSubmit={onSubmit}
            initialValues={initialValues}
            render={({
              handleSubmit,
              submitting,
              submitSucceeded,
              submitFailed,
              pristine,
              submitErrors,
              errors,
            }) => {
              let errorArr = [];
              for (let key in submitErrors) {
                errorArr.push(submitErrors[key]);
              }

              if (submitSucceeded || userContext.isAuth) {
                return <Redirect to="/register/edit" />;
              }

              return (
                <>
                  <form
                    id="branchForm"
                    css={{
                      width: '100%',
                      display: 'flex',
                      flexFlow: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onSubmit={handleSubmit}>
                    <AuthenticationInput
                      name="name"
                      type="name"
                      placeholder="'John' or whatever"
                      label="Name"
                    />
                    <AuthenticationInput
                      name="email"
                      type="email"
                      placeholder="email@address.com"
                      label="Email"
                    />
                    <AuthenticationInput
                      name="password1"
                      type="password"
                      placeholder="shhh..."
                      label="Password"
                    />
                    <AuthenticationInput
                      name="password2"
                      type="password"
                      placeholder="Confirm shhh..."
                      label="Confirm Password"
                    />

                    {submitErrors
                      ? errorArr.map((value) => {
                          return value.map((e) => {
                            return (
                              <div
                                key={e}
                                className="setting-error"
                                css={{margin: '15px 0', textAlign: 'center'}}>
                                {e}
                              </div>
                            );
                          });
                        })
                      : null}
                    {loading ? (
                      <MoonLoader
                        sizeUnit={'px'}
                        size={20}
                        color={'#123abc'}
                        loading={true}
                      />
                    ) : (
                      <AuthenicationSave value="Sign up" />
                    )}
                  </form>
                  <div
                    css={{
                      display: 'flex',
                      flexFlow: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginTop: 20,
                    }}>
                    <div css={{fontSize: '1.5rem'}}>
                      <span>Already have an account? </span>
                      <Link
                        to="/login"
                        css={(theme) =>
                          css({color: '#4b9be0', textDecoration: 'none'})
                        }>
                        Login
                      </Link>
                    </div>
                  </div>
                </>
              );
            }}
          />
        </div>
      </AuthenticationWrapper>
    </>
  );
}
