import React, {Component, useContext, useEffect, useState} from 'react';
import {createPortal} from 'react-dom';
import {Link} from 'react-router-dom';
import {css} from '@emotion/core';
import history from '../../history';
import {UserContext} from '../container/ContextContainer';
import {PopUp} from './PreviewPost';

export function UserRankBoard({shown, setShown}) {
  const userContext = useContext(UserContext);

  return createPortal(
    <PopUp shown={shown} setShown={setShown} header="Ownerships" zIndex={1004}>
      <div
        css={{
          width: '100%',
          display: 'flex',
          flexFlow: 'column',
          padding: 10,
          boxSizing: 'border-box',
        }}>
        {userContext.currentBranch.rankings.length == 0 ? (
          <div css={{width: '100%', margin: '20px 0'}}>
            <span css={{fontSize: '1.6rem', fontWeight: 500}}>
              You currently don&apos;t own a part of any community. Be active in
              communities to obtain ownerships!
            </span>
          </div>
        ) : (
          userContext.currentBranch.rankings.map((r, i) => {
            return (
              <div
                key={i}
                css={{display: 'flex', flexFlow: 'column', margin: '20px 0'}}>
                <div
                  css={{
                    fontSize: '1.5rem',
                    display: 'flex',
                    justifyContent: 'start',
                    alignItems: 'center',
                    margin: '10px 0',
                  }}>
                  <img
                    src={r.image}
                    css={{
                      height: 30,
                      width: 30,
                      objectFit: 'cover',
                      borderRadius: '50%',
                      marginRight: 10,
                    }}
                  />
                  <span css={{fontWeight: 'bold'}}>{r.community}</span>
                </div>
                <div
                  css={{
                    display: 'flex',
                    flexFlow: 'column',
                    fontSize: '1.6rem',
                  }}>
                  <span>
                    You currently own <b>{r.ownership.toFixed(1)}%</b> of this
                    community
                  </span>
                  <span>
                    Points: <b>{Math.round(r.rank)}</b> (
                    <b>#{r.rank_position}</b> poster)
                  </span>
                </div>
              </div>
            );
          })
        )}
      </div>
    </PopUp>,
    document.getElementById('modal-root'),
  );
}

export function CommunityBoard({shown, setShown}) {
  const userContext = useContext(UserContext);

  return createPortal(
    <PopUp shown={shown} setShown={setShown} header="Ownerships" zIndex={1004}>
      <div css={{display: 'flex', justifyContent: 'space-evenly'}}>
        <div
          css={{
            display: 'flex',
            flexFlow: 'column',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <h1>Community</h1>
          {userContext.currentBranch.rankings.map((r, i) => {
            return (
              <div
                key={i}
                css={{
                  fontSize: '1.5rem',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'start',
                  margin: '10px 0',
                }}>
                <img
                  src={r.image}
                  css={{
                    height: 30,
                    width: 30,
                    objectFit: 'cover',
                    borderRadius: '50%',
                    marginRight: 10,
                  }}
                />
                <span>{r.community}</span>
              </div>
            );
          })}
        </div>
        <div
          css={{
            display: 'flex',
            flexFlow: 'column',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <h1>Rank</h1>
          {userContext.currentBranch.rankings.map((r, i) => {
            return (
              <div key={i} css={{fontSize: '1.5rem'}}>
                {Math.round(r.rank)}
              </div>
            );
          })}
        </div>
      </div>
    </PopUp>,
    document.getElementById('modal-root'),
  );
}
