import React, {Component, useContext, useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {css} from '@emotion/core';
import history from '../../history';
import {useTheme} from '../container/ThemeContainer';
import {useMediaQuery} from 'react-responsive';
import {UserContext} from '../container/ContextContainer';
import {BranchSwitcher} from './BranchSwitcher';
import {ProfileBubble} from './MobileParentBranch';
import {Name, Uri, FollowInfo} from './ProfileViewer';
import {FollowButton} from './Card';

const profileImage = (theme) =>
  css({
    border: `4px solid ${theme.backgroundLightColor}`,
    borderRadius: '50%',
    width: 100,
    height: 100,
    objectFit: 'cover',
    '@media (max-device-width:767px)': {
      height: 70,
      width: 70,
    },
  });

const similar = (theme, isMobile) =>
  css({
    fontSize: isMobile ? '1.3em' : '1.6em',
    fontWeight: 'bold',
    padding: '10px 20px',
    backgroundColor: theme.backgroundLightOppositeColor,
    color: theme.backgroundColor,
    borderRadius: 100,
    display: 'block',
    boxSizing: 'border-box',
    textAlign: 'center',
    pointer: 'cursor',
    textDecoration: 'none',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  });

export function FlatProfile({branch}) {
  const userContext = useContext(UserContext);
  const isMobile = useMediaQuery({
    query: '(max-device-width: 767px)',
  });

  let branchType;
  if (branch.branch_type == 'CM') {
    branchType = 'Community';
  } else if (branch.branch_type == 'US') {
    branchType = 'Person';
  } else {
    branchType = 'Hub';
  }
  return (
    <div css={{display: 'flex', alignItems: 'center', flexFlow: 'column'}}>
      <div
        css={{
          display: 'flex',
          flexFlow: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <div css={{display: 'flex'}}>
          <h1 css={(theme) => ({margin: '5px 0', color: theme.textLightColor})}>
            {branchType}
          </h1>
        </div>

        <img css={profileImage} src={branch.branch_image} />
        <div
          css={{
            display: 'flex',
            flexFlow: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            '@media (max-device-width:767px)': {
              fontSize: '0.7rem',
            },
          }}>
          <Name name={branch.name} />
          <Uri uri={branch.uri} />
        </div>
      </div>
      <div
        css={{
          display: 'flex',
          flexFlow: 'column',
          flexGrow: 1,
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '80%',
        }}>
        <div
          css={(theme) => ({
            margin: 10,
            marginTop: 20,
            width: '100%',
            borderRadius: 50,
            height: 5,
            width: '100%',
            maxWidth: 400,
            backgroundColor: theme.backgroundLightColor,
          })}></div>
        <span css={{fontSize: '1.7rem'}}>{branch.description}</span>
        <div css={{maxWidth: 200, width: '100%', marginTop: 10}}>
          <FollowInfo branch={branch} />
        </div>
        <div
          css={{
            width: '100%',
            display: 'flex',
            marginTop: 10,
            flexFlow: 'row wrap',
          }}>
          <div
            css={{
              flexGrow: 1,
              display: 'flex',
              justifyContent: 'space-evenly',
            }}>
            {userContext.isAuth &&
            branch.uri != userContext.currentBranch.uri ? (
              <FollowButton
                branch={branch}
                style={{
                  margin: 0,
                  width: isMobile ? 80 : 140,
                  margin: 0,
                  fontSize: isMobile ? '1.3em' : '1.6em',
                }}
              />
            ) : null}

            <Link
              to={`/${branch.uri}/branches`}
              css={(theme) => similar(theme, isMobile)}>
              {branch.branch_count} similar
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
