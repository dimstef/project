import React from 'react';
import {css} from '@emotion/core';
import history from '../../history';

function CrashPage() {
  return (
    <div
      css={{
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexFlow: 'column',
      }}>
      <img
        src="https://sb-static.s3.eu-west-2.amazonaws.com/static/static/android-chrome-256x256.png"
        css={{height: 100, width: 100}}
        onClick={() => history.push('/')}
      />
      <h1 css={{textAlign: 'center', maxWidth: 300}}>
        Something must have went terribly wrong :(
      </h1>
      <button className="accept-btn" onClick={() => location.reload()}>
        Refresh this page
      </button>
    </div>
  );
}

export default CrashPage;
