import React, {
  Component,
  useContext,
  useEffect,
  useLayoutEffect,
  useState,
  useRef,
} from 'react';
import {
  Link,
  Redirect,
  Route,
  Switch,
  withRouter,
  useParams,
  useRouteMatch,
  useLocation,
} from 'react-router-dom';
import {Global, css} from '@emotion/core';
import {withTheme, useTheme as useEmotionTheme} from 'emotion-theming';
import {useTheme} from '../container/ThemeContainer';
import {useMediaQuery} from 'react-responsive';
import {Helmet} from 'react-helmet';
import {Page} from '../Page';
import Login from './Login';
import Logout from './Logout';
import Register from './Register';
import PostRegister from './PostRegister';
import PasswordReset from './PasswordReset';
import PasswordResetConfirm from './PasswordResetConfirm';
import EmailConfirm from './EmailConfirm';
import {
  BranchCommunityPostsContext,
  BranchPostsContext,
  BranchTreePostsContext,
  NotificationsProvider,
  SingularPostContext,
  TourContext,
  UserContext,
  RouteTransitionContext,
  ParentBranchDrawerContext,
  UserActionsContext,
} from '../container/ContextContainer';
import {ChatRoomsContainer} from '../container/ChatRoomsContainer';
import {ChatRoomSettings} from './ChatRoomSettings';
import {CreateNewChat} from './CreateNewChat';
import {GenericBranchPosts} from './BranchPosts';
import {
  TrendingContainer,
  TrendingWithWrapper as Trending,
} from '../container/TrendingContainer';
import Card from './Card';
import Responsive from 'react-responsive';
import {ResponsiveNavigationBar} from './Navigation';
import {NotificationsContainer} from './Notifications';
import {SearchPage} from './SearchPage';
import {SettingsPage} from './SettingsPage';
import {SingularPost} from './SingularPost';
import {
  CSSTransition,
  TransitionGroup,
  Transition,
} from 'react-transition-group';
import {FrontPage, FrontPageLeftBar} from './FrontPage';
import {
  MobileBranchPageWrapper,
  ProfileBubble,
  MobileParentBranch2,
} from './MobileParentBranch';
import {BranchLinks, PostLinks} from './GoogleLinks';
import {FeedbackPage} from './FeedbackPage';
import {DiscoverBranchesPage} from './NavigateBranchesPage';
import {DesktopProfile} from './ProfileViewer';
import {FollowPage} from './FollowPage';
import {matchPath} from 'react-router';
import pathToRegexp from 'path-to-regexp';
import history from '../../history';
import axiosRetry from 'axios-retry';
import axios from 'axios';

const Desktop = (props) => <Responsive {...props} minDeviceWidth={1224} />;
const Tablet = (props) => (
  <Responsive {...props} minDeviceWidth={768} maxDeviceWidth={1223} />
);
const Mobile = (props) => <Responsive {...props} maxDeviceWidth={767} />;

const Routes = () => {
  function updateTour() {
    localStorage.setItem('has_seen_tour', true);
  }

  useEffect(() => {
    window.addEventListener('beforeunload', updateTour);

    return () => {
      window.removeEventListener('beforeunload', updateTour);
    };
  }, []);
  return (
    <Switch>
      <Route
        exact
        path="/logout/:instant(instant)?"
        component={(props) => <Logout {...props} />}
      />
      <Route exact path="/login" component={(props) => <Login {...props} />} />
      <Route
        exact
        path="/register"
        component={(props) => <Register {...props} />}
      />
      <Route
        exact
        path="/register/edit"
        render={(props) => <PostRegister {...props} />}
      />
      <Route exact path="/password/reset" component={PasswordReset} />
      <Route
        exact
        path="/reset/:uid/:token"
        component={(props) => <PasswordResetConfirm {...props} />}
      />
      <Route
        path="/accounts/confirm-email/:token"
        component={(props) => <EmailConfirm {...props} />}
      />
      <Page>
        <NonAuthenticationRoutes />
      </Page>
    </Switch>
  );
};

const makeGlobalStyles = (theme, isMobileOrTablet) => css`
  html{
    overflow:auto;
    overscroll-behavior: none;
  }
  
  body {
    background: ${theme.backgroundColor};
    color: ${theme.textColor};
    -webkit-overflow-scrolling: touch;
  }
  
  /**::-webkit-scrollbar{
    width:10px;
  }

  @media all and (max-device-width:767px){
    *::-webkit-scrollbar{
      width:4px;
    }
  }
  *::-webkit-scrollbar-thumb{
    background-color:${theme.scrollBarColor};
  }*/
`;

const GlobalStyles = withTheme(({theme}) => {
  const isMobileOrTablet = useMediaQuery({
    query: '(max-device-width: 1223px)',
  });
  return <Global styles={makeGlobalStyles(theme, isMobileOrTablet)} />;
});

const RoutesWrapper = (props) => {
  const [messages, setMessages] = useState([]);
  const [notifications, setNotifications] = useState([]);

  return (
    <NotificationsProvider
      value={{
        messages: messages,
        setMessages: setMessages,
        notifications: notifications,
        setNotifications: setNotifications,
      }}>
      <GlobalStyles />
      <Routes />
    </NotificationsProvider>
  );
};

const AnimatedSwitch = React.memo(function ({
  animationClassName,
  animationTimeout,
  children,
}) {
  const isMobileOrTablet = useMediaQuery({
    query: '(max-device-width: 1223px)',
  });

  const location = useLocation();
  const prevPath = useRef(null);
  const transitionContext = useContext(RouteTransitionContext);

  // This prevents static routes like "/notifications" to be remounted when clicked from the navigation bar
  // state is defined in each Link
  let key = location.state || location.key;

  function onExiting() {
    transitionContext.entered = false;
    transitionContext.exiting = true;
  }

  function onExited() {
    transitionContext.exiting = false;
  }

  function onEntered() {
    transitionContext.exiting = false;
    transitionContext.entered = true;
  }

  function onEnter() {
    transitionContext.entered = false;
  }

  React.useEffect(() => {
    try {
      window.scrollTo({top: 0, behavior: 'smooth'});
    } catch (e) {
      window.scrollTo(0, 0);
    }
  }, [location]);

  return isMobileOrTablet ? (
    <TransitionGroup component={null}>
      <CSSTransition
        key={key}
        timeout={animationTimeout}
        classNames={animationClassName}
        onExiting={onExiting}
        onExited={onExited}
        onEnter={onEnter}
        onEntered={onEntered}>
        <Switch location={location}>{children}</Switch>
      </CSSTransition>
    </TransitionGroup>
  ) : (
    <Switch location={location}>{children}</Switch>
  );
});

if (process.env.NODE_ENV !== 'production') {
  const whyDidYouRender = require('@welldone-software/why-did-you-render');
  whyDidYouRender(React);
}

//AnimatedSwitch.whyDidYouRender = true;

const AnimatedRoute = (props) => {
  const isMobileOrTablet = useMediaQuery({
    query: '(max-device-width: 1223px)',
  });

  let mobileCss = isMobileOrTablet
    ? {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
      }
    : {};
  return (
    <div
      id="transition-container"
      className={isMobileOrTablet ? 'mobile-transition-container' : ''}
      css={{
        ...mobileCss,
        display: isMobileOrTablet ? 'block' : 'flex',
        width: '100%',
      }}>
      <Route {...props} />
    </div>
  );
};

export default withRouter(RoutesWrapper);

function NonAuthenticationRoutes() {
  const userContext = useContext(UserContext);
  const tourContext = useContext(TourContext);

  useEffect(() => {
    if (userContext.isAuth && !userContext.user.profile.has_seen_tour) {
      const data = new FormData();
      data.append('has_seen_tour', true);
      const url = `/api/user_profile/${userContext.user.profile.id}/`;
      const request = axios
        .patch(url, data, {
          headers: {
            'Content-Type': 'multipart/form-data',
            'X-CSRFToken': getCookie('csrftoken'),
          },
          withCredentials: true,
        })
        .then((response) => {
          //
        })
        .catch((error) => {});
    }
  }, []);

  return (
    <>
      <Switch
        animationClassName="pages"
        animationTimeout={{enter: 300, exit: 300}}>
        <AnimatedRoute
          exact
          path="/google/links/branches/:pageNumber?"
          component={(props) => <BranchLinks {...props} />}
        />
        <AnimatedRoute
          exact
          path="/google/links/posts/:pageNumber?"
          component={(props) => <PostLinks {...props} />}
        />
        <AnimatedRoute
          path="/settings"
          render={() =>
            userContext.isAuth ? <SettingsPage /> : <Redirect to="/login" />
          }
        />
        <AnimatedRoute
          exact
          path="/:page(all|tree)?"
          render={(props) => <FrontPage {...props} />}
        />
        <AnimatedRoute
          path="/search/:uri"
          render={(props) => (
            <DiscoverBranchesPage {...props} endpoint="search" />
          )}
        />
        <AnimatedRoute path="/search" component={SearchPage} />
        <AnimatedRoute path="/feedback" component={FeedbackPage} />
        <AnimatedRoute
          path="/notifications"
          render={() =>
            userContext.isAuth ? (
              <NotificationsContainer />
            ) : (
              <Redirect to="/login" />
            )
          }
        />
        <AnimatedRoute
          exact
          path="/messages/create_conversation"
          render={(props) =>
            userContext.isAuth ? (
              <CreateNewChat {...props} />
            ) : (
              <Redirect to="/login" />
            )
          }
        />
        <AnimatedRoute
          exact
          path="/messages/:roomName/:page(invite|settings)"
          render={(props) =>
            userContext.isAuth ? (
              <ChatRoomSettings {...props} />
            ) : (
              <Redirect to="/login" />
            )
          }
        />
        <AnimatedRoute
          path="/messages/:roomName?"
          render={(props) =>
            userContext.isAuth ? (
              <ChatRoomsContainer {...props} />
            ) : (
              <Redirect to="/login" />
            )
          }
        />

        <AnimatedRoute
          exact
          path="/:uri/leaves/:externalId"
          render={({match}) => (
            <SingularPostWrapper externalPostId={match.params.externalId} />
          )}
        />
        <AnimatedRoute
          exact
          path={`/:uri/followers`}
          component={(props) => <FollowPage {...props} type="followed_by" />}
        />
        <AnimatedRoute
          exact
          path={`/:uri/following`}
          component={(props) => <FollowPage {...props} type="following" />}
        />
        <AnimatedRoute
          path="/:uri?"
          render={(props) => <BranchContainer {...props} />}
        />
      </Switch>
    </>
  );
}

export const BranchContainer = React.memo(function BranchContainer(props) {
  const actionContext = useContext(UserActionsContext);
  const [branch, setBranch] = useState(null);
  const [loaded, setLoaded] = useState(false);
  let branchUri = props.match.params.uri;

  async function getBranch(branchUri) {
    let uri;

    uri = `/api/branches/${branchUri}/`;
    try {
      let parentResponse = await axios.get(uri);
      let parentData = parentResponse.data;
      setBranch(parentData);
    } catch (err) {
      //console.log(err.response)
    }
    setLoaded(true);
  }

  useEffect(() => {
    actionContext.lastPostListType = 'branch';
  }, []);

  useEffect(() => {
    getBranch(branchUri);
  }, [branchUri]);

  if (loaded) {
    if (branch) {
      return (
        <BranchPage
          externalPostId={props.match.params.externalPostId}
          branch={branch}
          match={branchUri}
        />
      );
    } else {
      return (
        <>
          <Helmet>
            <title>Branch not found - Westeria</title>
            <meta name="description" content="Branch not found." />
          </Helmet>
          <p
            style={{
              margin: '0 auto',
              fontSize: '3rem',
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            Nothing seems to be here
          </p>
        </>
      );
    }
  } else {
    return null;
  }
});

function NonAuthenticationColumn() {
  return (
    <div
      className="box-border flex-fill"
      style={{
        padding: '10px 20px',
        alignItems: 'center',
        WebkitAlignItems: 'center',
        flexFlow: 'column',
        WebkitFlexFlow: 'column',
      }}>
      <Link to="/login" className="login-or-register">
        Login
      </Link>
      <span style={{fontSize: '1.4rem', color: '#a4a5b2'}}>or</span>
      <Link to="/register" className="login-or-register">
        Register
      </Link>
    </div>
  );
}

if (process.env.NODE_ENV !== 'production') {
  const whyDidYouRender = require('@welldone-software/why-did-you-render');
  whyDidYouRender(React);
}

const postWrapper = (theme) =>
  css({
    border: `1px solid ${theme.borderColor}`,
    '@media (max-device-width:767px)': {
      maxWidth: '100%',
    },
    '@media (min-device-width:768px)': {
      maxWidth: '70%',
      margin: '0 auto',
    },
  });

function SingularPostWrapper({externalPostId}) {
  const singularPostContext = useContext(SingularPostContext);
  const userContext = useContext(UserContext);

  return (
    <>
      <Desktop>
        <DesktopParentBranchWrapper branch={userContext.currentBranch}>
          <div className="post-list" css={(theme) => postWrapper(theme)}>
            <SingularPost
              postId={externalPostId}
              postsContext={singularPostContext}
              activeBranch={userContext.currentBranch}
            />
          </div>
        </DesktopParentBranchWrapper>
      </Desktop>

      <Tablet>
        <div className="post-list" css={(theme) => postWrapper(theme)}>
          <SingularPost
            postId={externalPostId}
            postsContext={singularPostContext}
            activeBranch={userContext.currentBranch}
          />
        </div>
      </Tablet>

      <Mobile>
        <div className="post-list" css={(theme) => postWrapper(theme)}>
          <SingularPost
            postId={externalPostId}
            postsContext={singularPostContext}
            activeBranch={userContext.currentBranch}
          />
        </div>
      </Mobile>
    </>
  );
}

const rightBar = (theme) =>
  css({
    backgroundColor: theme.backgroundColor,
    color: theme.textColor,
    flexBasis: '22%',
    height: 'max-content',
  });

const rightBarP = (theme) =>
  css({
    fontSize: '1.6em',
    fontWeight: 600,
    paddingBottom: 5,
    margin: '-10px -20px',
    backgroundColor: '#219ef3',
    color: theme.textColor,
    padding: '10px 20px',
    marginBottom: 10,
  });

function FrontPageRightBar() {
  return (
    <div css={(theme) => rightBar(theme)}>
      <div className="box-border" style={{padding: '10px 20px'}}>
        <p css={(theme) => rightBarP(theme)}>Popular now</p>
        <TrendingContainer />
      </div>
    </div>
  );
}

function ResponsiveBranchPage({branch, children}) {
  return (
    <>
      <Desktop>
        <DesktopParentBranchWrapper branch={branch}>
          {children}
        </DesktopParentBranchWrapper>
      </Desktop>
      <Tablet>
        <MobileParentBranch2 branch={branch}>{children}</MobileParentBranch2>
      </Tablet>
      <Mobile>
        <MobileParentBranch2 branch={branch}>{children}</MobileParentBranch2>
      </Mobile>
    </>
  );
}

function DesktopParentBranchWrapper(props) {
  return (
    <div className="flex-fill" style={{flexFlow: 'row', width: '100%'}}>
      <ProfileBubble branch={props.branch} />
      {props.children}
    </div>
  );
}

export const BranchPage = function (props) {
  return (
    <ResponsiveBranchPage branch={props.branch}>
      <Helmet>
        <title>
          {props.branch.name} (@{props.branch.uri}) - Westeria
        </title>
        <meta name="description" content={props.branch.description} />
        <link
          rel="canonical"
          href={`${window.location.origin}/${props.branch.uri}`}></link>
      </Helmet>
      <Switch>
        <Route
          path={`/${props.match}/branches`}
          render={() => (
            <DiscoverBranchesPage
              {...props}
              showTop={false}
              withHeadline
              endpoint="branches"
            />
          )}
        />
        <Route
          path={`/${props.match}/:keyword(self|community|tree)?/`}
          render={({match}) => (
            <BranchFrontPage {...props} keywordMatch={match} />
          )}
        />
      </Switch>
    </ResponsiveBranchPage>
  );
};

function BranchFrontPage(props) {
  let _context = null;

  let keyword = props.keywordMatch.params.keyword;
  let defaultKeyword;
  let defaultContext;
  if (props.branch.branch_type == 'US' || props.branch.branch_type == 'HB') {
    defaultKeyword = null;
    defaultContext = BranchPostsContext;
  } else if (props.branch.branch_type == 'CM') {
    defaultKeyword = 'community';
    defaultContext = BranchCommunityPostsContext;
  }

  useLayoutEffect(() => {
    if (!keyword && postsContext.useDefaultRoute) {
      if (props.branch.branch_type == 'CM') {
        postsContext.useDefaultRoute = false;
        history.push(`/${props.branch.uri}/community`);
      }
    }
  }, []);

  if (keyword == 'community') {
    _context = BranchCommunityPostsContext;
  } else if (keyword == 'tree') {
    _context = BranchTreePostsContext;
  } else {
    _context = BranchPostsContext;
  }

  const postsContext = useContext(_context);
  const userContext = useContext(UserContext);

  var uri;

  uri = `/api/branches/${props.match}/posts/`;

  useEffect(() => {
    return () => {
      let lastVisibleElements = document.querySelectorAll(
        '[data-visible="true"]',
      );
      postsContext.lastVisibleElement = lastVisibleElements[0];
      postsContext.lastVisibleIndex = lastVisibleElements[0]
        ? lastVisibleElements[0].dataset.index
        : 0;
    };
  }, [keyword]);

  return (
    <>
      <Desktop>
        <div css={{flex: 1}}>
          <div>
            <div className="flex-fill" style={{width: '100%'}}>
              {props.externalPostId ? (
                <SingularPost
                  postId={props.externalPostId}
                  postsContext={postsContext}
                  activeBranch={userContext.currentBranch}
                />
              ) : (
                <BranchPosts
                  {...props}
                  postsContext={postsContext}
                  keyword={keyword}
                  branch={props.match}
                  activeBranch={props.branch}
                  postedId={props.branch.id}
                  postingTo={props.branch}
                  uri={uri}
                />
              )}
            </div>
          </div>
        </div>
      </Desktop>
      <Tablet>
        <BranchPosts
          {...props}
          postsContext={postsContext}
          keyword={keyword}
          branch={props.match}
          activeBranch={props.branch}
          postedId={props.branch.id}
          postingTo={props.branch}
          uri={uri}
        />
      </Tablet>
      <Mobile>
        <BranchPosts
          {...props}
          postsContext={postsContext}
          keyword={keyword}
          branch={props.match}
          activeBranch={props.branch}
          postedId={props.branch.id}
          postingTo={props.branch}
          uri={uri}
        />
      </Mobile>
    </>
  );
}

const postList = (theme) =>
  css({
    flexBasis: '56%',
    width: '100%',
    padding: 0,
    listStyle: 'none',
    border: `1px solid ${theme.borderColor}`,
  });

function BranchPosts(props) {
  return (
    <div className="post-list" id="post-list" css={(theme) => postList(theme)}>
      {/*<BranchPagePostList branch={props.activeBranch}/>*/}
      <GenericBranchPosts {...props} />
    </div>
  );
}
