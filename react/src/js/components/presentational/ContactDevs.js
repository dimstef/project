import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {ToggleContent, Modal} from './Temporary';
import {InfoSvg} from './Svgs';

function ContactDevs({showContact, setShowContact}) {
  return (
    <ToggleContent
      toggle={(show) => (
        <div
          role="button"
          onClick={() => {
            setShowContact(true);
            show();
          }}
          css={(theme) => ({
            backgroundColor: theme.postFloatingButtonColor,
            boxShadow: theme.postFloatingButtonShadow,
            padding: 20,
            boxSizing: 'border-box',
            height: 50,
            width: '90%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            fontSize: '1.5rem',
            borderRadius: 5,
            fontWeight: 'bold',
            cursor: 'pointer',
          })}>
          <p css={{margin: 0}}>Contact the devs</p>
          <InfoSvg
            css={(theme) => ({
              fill: theme.textColor,
              height: 20,
              width: 20,
              marginLeft: 10,
            })}
          />
        </div>
      )}
      content={(hide) => (
        <Modal
          onClick={() => setShowContact(false)}
          hide={hide}
          isOpen={showContact}
          portalElement="hidden-elements">
          <div
            css={{
              position: 'fixed',
              top: 0,
              width: '100vw',
              height: '100%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <div
              onClick={(e) => e.stopPropagation()}
              css={(theme) => ({
                backgroundColor: theme.backgroundLightColor,
                borderRadius: 25,
                width: 600,
                padding: 20,
                boxSizing: 'border-box',
                fontSize: '1.8rem',
              })}>
              <h2>Contact the devs</h2>
              <p>
                You can directly contact the Westeria team in a number of ways
              </p>
              <ol>
                <li>
                  <p>Join the Westeria conversation you have been invited to</p>
                  <ul>
                    <li>Click the messages tab</li>
                    <li>
                      Click <b>join</b> on the room named{' '}
                      <b>Talk with the Westeria team</b>
                    </li>
                  </ul>
                </li>
                <li>
                  <p>
                    Send us an email at <b>admin@westeria.app</b>
                  </p>
                </li>
                <li>
                  <p>Send a message on our social media</p>
                  <ul>
                    <li>
                      <a
                        target="_blank"
                        rel="noreferrer"
                        href="https://twitter.com/westeria_off"
                        style={{color: '#2196f3', textDecoration: 'none'}}>
                        Twitter
                      </a>
                    </li>
                    <li>
                      <a
                        target="_blank"
                        rel="noreferrer"
                        href="https://www.facebook.com/Westeria-105318627644413"
                        style={{color: '#2196f3', textDecoration: 'none'}}>
                        Facebook
                      </a>
                    </li>
                    <li>
                      <a
                        target="_blank"
                        rel="noreferrer"
                        href="https://www.instagram.com/westeriaofficial/"
                        style={{color: '#2196f3', textDecoration: 'none'}}>
                        Instagram
                      </a>
                    </li>
                  </ul>
                </li>
              </ol>
            </div>
          </div>
        </Modal>
      )}
    />
  );
}

export default ContactDevs;
