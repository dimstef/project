from rest_framework import permissions
from branches.models import Branch
from branchchat.models import BranchChat


class IsOwnerOfPost(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return True
        branches = request.user.owned_groups.all()
        for branch in branches:
            if branch.posts.all().filter(id=obj.id):
                return True
        return False


class IsOwnerOfReply(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        branches = request.user.owned_groups.all()
        for branch in branches:
            if branch.posts.all().filter(id=request.data['replies'][0]):
                return True
        return False


class IsOwnerOfBranch(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):

        lookup = 'uri'
        if 'branch__uri' in request.resolver_match.kwargs:
            lookup = 'branch__uri'
        elif 'branch_uri' in request.resolver_match.kwargs:
            lookup = 'branch_uri'

        branch = Branch.objects.get(uri=request.resolver_match.kwargs.get(lookup))
        if request.user.owned_groups.filter(uri=branch).exists():
            return True
        return False


class IsMemberOfChat(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        lookup_branch = 'uri'
        if 'branch__uri' in request.resolver_match.kwargs:
            lookup_branch = 'branch__uri'
        elif 'branch_uri' in request.resolver_match.kwargs:
            lookup_branch = 'branch_uri'

        lookup_id = 'pk'
        if 'id__pk' in request.resolver_match.kwargs:
            lookup_id = 'id__pk'
        elif 'id_pk' in request.resolver_match.kwargs:
            lookup_id = 'id_pk'

        branch = Branch.objects.get(uri=request.resolver_match.kwargs.get(lookup_branch))
        branch_chat = BranchChat.objects.get(id=request.resolver_match.kwargs.get(lookup_id))
        if branch_chat.members.filter(uri=branch).exists():
            return True
        return False


class IsOwnerOfChat(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        lookup_branch = 'uri'
        if 'branch__uri' in request.resolver_match.kwargs:
            lookup_branch = 'branch__uri'
        elif 'branch_uri' in request.resolver_match.kwargs:
            lookup_branch = 'branch_uri'

        lookup_id = 'pk'
        if 'id__pk' in request.resolver_match.kwargs:
            lookup_id = 'id__pk'
        elif 'id_pk' in request.resolver_match.kwargs:
            lookup_id = 'id_pk'

        chat_id = request.resolver_match.kwargs.get(lookup_id)
        branch = Branch.objects.get(uri=request.resolver_match.kwargs.get(lookup_branch))
        branch_chat = BranchChat.objects.get(id=chat_id)

        if branch_chat.owner.owner.owned_groups.filter(id=branch.id).exists():
            return True
        return False