from django.db import models
from django.db.models import Window, F, Sum
from django.db.models.signals import post_save,m2m_changed
from django.dispatch import receiver
from branchposts.models import Post
from branches.models import Branch
from branchranking.models import BranchRanking
import uuid


class RemovePostAction(models.Model):

    surrogate = models.UUIDField(default=uuid.uuid4,unique=True,db_index=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="removal_voting")
    from_community = models.ForeignKey(Branch,on_delete=models.CASCADE, related_name="removal_votings")
    passed = models.BooleanField(default=False)
    initiated_by = models.ForeignKey(Branch,on_delete=models.CASCADE,blank=True,
                                     null=True, related_name="removal_votings_initiated")
    agreed = models.ManyToManyField(Branch, blank=True, related_name="removal_votings_agreed")
    disagreed = models.ManyToManyField(Branch, blank=True, related_name="removal_votings_disagreed")

    @property
    def agree_percentage(self):
        community_score = BranchRanking.objects.filter(community=self.from_community).aggregate(Sum('rank'))
        agreed_score = BranchRanking.objects. \
            filter(community=self.from_community, ranked_branch__in=self.agreed.all()) \
            .aggregate(Sum('rank'))
        if community_score['rank__sum'] and agreed_score['rank__sum']:
            return (agreed_score['rank__sum']/community_score['rank__sum'])*100
        else:
            return 0


@receiver(post_save, sender=RemovePostAction, dispatch_uid="remove_post")
def remove_post(sender, instance,created, **kwargs):
    if not created:
        if instance.agree_percentage > 50:
            if instance.post.posted_to.count() > 1 and instance.from_community!=instance.post.poster:
                instance.post.posted_to.remove(instance.from_community)
            else:
                instance.post.delete()