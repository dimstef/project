from django.contrib import admin
from .models import RemovePostAction

# Register your models here.
class RemovePostActionAdmin(admin.ModelAdmin):
    filter_horizontal = ('agreed','disagreed',)


admin.site.register(RemovePostAction, RemovePostActionAdmin)
#admin.site.register(RemovePostAction)